USE [CollegeWizard]
GO
/****** Object:  Table [dbo].[Customer]    Script Date: 30/12/19 10:03:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer](
	[CustomerID] [int] NOT NULL,
	[CompanyName] [nchar](10) NULL,
	[ContactName] [nchar](10) NULL,
	[Address] [nchar](10) NULL,
	[City] [nchar](10) NULL,
	[Country] [nchar](10) NULL,
	[PostalCode] [nchar](10) NULL,
	[Phone] [nchar](10) NULL,
	[Fax] [nchar](10) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[District]    Script Date: 30/12/19 10:03:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[District](
	[DId] [int] NULL,
	[DistrictName] [nvarchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Gender]    Script Date: 30/12/19 10:03:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Gender](
	[GId] [int] NULL,
	[GName] [nvarchar](10) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Job]    Script Date: 30/12/19 10:03:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Job](
	[PNo] [int] NOT NULL,
	[Office] [nchar](10) NULL,
	[Department] [nchar](10) NULL,
	[Salary] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PersonAddress]    Script Date: 30/12/19 10:03:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PersonAddress](
	[PNo] [int] NOT NULL,
	[Phone] [nchar](10) NULL,
	[Country] [nchar](10) NULL,
	[DistrictName] [nvarchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PersonDetail]    Script Date: 30/12/19 10:03:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PersonDetail](
	[PNo] [int] NOT NULL,
	[Name] [nchar](10) NULL,
	[GName] [nchar](10) NULL,
PRIMARY KEY CLUSTERED 
(
	[PNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[StudentDetail]    Script Date: 30/12/19 10:03:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StudentDetail](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nchar](10) NULL,
	[Address] [nchar](10) NULL,
	[Phone] [nchar](10) NULL,
 CONSTRAINT [PK_StudentDetail] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Students]    Script Date: 30/12/19 10:03:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Students](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[City] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.Students] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Study]    Script Date: 30/12/19 10:03:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Study](
	[PNo] [int] NOT NULL,
	[School] [nchar](10) NULL,
	[College] [nchar](10) NULL
) ON [PRIMARY]
GO
INSERT [dbo].[Customer] ([CustomerID], [CompanyName], [ContactName], [Address], [City], [Country], [PostalCode], [Phone], [Fax]) VALUES (1, N'Apple     ', N'orton     ', N'Kalanki   ', N'Kathmandu ', N'Nepal     ', N'6595      ', N'9840014128', N'558985    ')
INSERT [dbo].[District] ([DId], [DistrictName]) VALUES (1, N'Kathmandu')
INSERT [dbo].[District] ([DId], [DistrictName]) VALUES (2, N'Bhaktapur')
INSERT [dbo].[District] ([DId], [DistrictName]) VALUES (3, N'Lalitpur')
INSERT [dbo].[Gender] ([GId], [GName]) VALUES (1, N'Female')
INSERT [dbo].[Gender] ([GId], [GName]) VALUES (2, N'Male')
INSERT [dbo].[Job] ([PNo], [Office], [Department], [Salary]) VALUES (5141, N'NLIC      ', N'IT        ', 500000)
INSERT [dbo].[PersonAddress] ([PNo], [Phone], [Country], [DistrictName]) VALUES (5082, N'9840014128', N'Nepal     ', N'Kathmandu')
INSERT [dbo].[PersonAddress] ([PNo], [Phone], [Country], [DistrictName]) VALUES (5141, N'9840014128', N'Nepal     ', N'Kathmandu')
INSERT [dbo].[PersonDetail] ([PNo], [Name], [GName]) VALUES (5043, N'Mahesh    ', N'Male      ')
INSERT [dbo].[PersonDetail] ([PNo], [Name], [GName]) VALUES (5082, N'Mahesh    ', N'Male      ')
INSERT [dbo].[PersonDetail] ([PNo], [Name], [GName]) VALUES (5141, N'Mahesh    ', N'Male      ')
SET IDENTITY_INSERT [dbo].[StudentDetail] ON 

INSERT [dbo].[StudentDetail] ([Id], [Name], [Address], [Phone]) VALUES (1, N'Rahul     ', N'Kalanki   ', N'9840014128')
INSERT [dbo].[StudentDetail] ([Id], [Name], [Address], [Phone]) VALUES (2, N'Rahul     ', N'Kalanki   ', N'9840014128')
INSERT [dbo].[StudentDetail] ([Id], [Name], [Address], [Phone]) VALUES (3, N'Saran     ', N'Kalanki   ', N'9855025369')
INSERT [dbo].[StudentDetail] ([Id], [Name], [Address], [Phone]) VALUES (4, NULL, N'Kalanki   ', N'9840014128')
INSERT [dbo].[StudentDetail] ([Id], [Name], [Address], [Phone]) VALUES (5, NULL, N'Pokhara   ', N'9855025369')
INSERT [dbo].[StudentDetail] ([Id], [Name], [Address], [Phone]) VALUES (6, NULL, N'Pokhara   ', N'9855025369')
INSERT [dbo].[StudentDetail] ([Id], [Name], [Address], [Phone]) VALUES (7, N'Mahesh    ', N'Kalanki   ', N'9840014128')
INSERT [dbo].[StudentDetail] ([Id], [Name], [Address], [Phone]) VALUES (8, N'Sahan     ', N'Pokhara   ', NULL)
SET IDENTITY_INSERT [dbo].[StudentDetail] OFF
SET IDENTITY_INSERT [dbo].[Students] ON 

INSERT [dbo].[Students] ([Id], [Name], [City], [PhoneNumber]) VALUES (1, N'Rahul Karn', N'Kathmandu', N'9840014128')
SET IDENTITY_INSERT [dbo].[Students] OFF
INSERT [dbo].[Study] ([PNo], [School], [College]) VALUES (5082, N'GSS       ', N'ACEM      ')
INSERT [dbo].[Study] ([PNo], [School], [College]) VALUES (5141, N'GSS       ', N'ACEM      ')
ALTER TABLE [dbo].[Job]  WITH CHECK ADD FOREIGN KEY([PNo])
REFERENCES [dbo].[PersonDetail] ([PNo])
GO
ALTER TABLE [dbo].[PersonAddress]  WITH CHECK ADD FOREIGN KEY([PNo])
REFERENCES [dbo].[PersonDetail] ([PNo])
GO
ALTER TABLE [dbo].[Study]  WITH CHECK ADD FOREIGN KEY([PNo])
REFERENCES [dbo].[PersonDetail] ([PNo])
GO
