﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CollegeWizard.Startup))]
namespace CollegeWizard
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
