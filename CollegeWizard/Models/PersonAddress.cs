﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CollegeWizard.Models
{
    public class PersonAddress
    {
        public int PNo { get; set; }
        [Required]
        public string Phone { get; set; }
        [Required]
        public string Country { get; set; }

        public District District { get; set; }
        [Required]
        public byte DId { get; set; }

        //xtrz\a
        [Required]
        public int Salary { get; set; }
    }
}