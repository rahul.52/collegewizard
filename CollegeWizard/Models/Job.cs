﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CollegeWizard.Models
{
    public class Job
    {
        public int PNo { get; set; }
        [Required]
        public string Office { get; set; }
        [Required]
        public string Department { get; set; }
        [Required]
        public int Salary { get; set; }

        
    }
}