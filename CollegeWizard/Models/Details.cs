﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CollegeWizard.Models
{
    public class Details
    {
        public string Address { get; set; }
        public string Phone { get; set; }
    }
}