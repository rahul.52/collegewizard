﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CollegeWizard.Models
{
    public class temp
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public int Salary { get; set; }
        [Required]
        public string School { get; set; }
        [Required]
        public string College { get; set; }
        [Required]
        public string Office { get; set; }
        [Required]
        public string Department { get; set; }
        [Required]
        public string Phone { get; set; }



        public string Country { get; set; }
    }
}