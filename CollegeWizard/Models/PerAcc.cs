﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CollegeWizard.Models
{
    public class PerAcc
    {
        public PersonDetail PersonDetail { get; set; }
        public PersonAddress PersonAddress { get; set; }
        public Job Job { get; set; }
        public Study Study { get; set; }

        [Required]
        public string Name { get; set; }
        [Required]
        public int Salary { get; set; }
        [Required]
        public string School { get; set; }
        [Required]
        public string College { get; set; }
        [Required]
        public string Office { get; set; }
        [Required]
        public string Department { get; set; }
        [Required]
        public string Phone { get; set; }
        [Required]
        public string Country { get; set; }

        public byte GId { get; set; }


        public byte DId { get; set; }
        public string DistrictName { get; set; }
    }
}
