﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CollegeWizard.Models
{
    public class Gender
    {
        public byte GId { get; set; }
        public string GName { get; set; }
    }
}