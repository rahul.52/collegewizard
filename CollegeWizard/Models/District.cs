﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CollegeWizard.Models
{
    public class District
    {
        public byte DId { get; set; }
        public string DistrictName { get; set; }
    }
}