﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CollegeWizard.Models
{
    public class PersonDetail
    {
        public int PNo { get; set; }

        [Display(Name = "Full Name")]
        public string Name { get; set; }
        [Display(Name = " Select Gender")]

        public Gender Gender { get; set; }
        public byte GId { get; set; }

    }
}