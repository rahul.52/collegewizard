﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CollegeWizard.Models
{
    public class Study
    {
        public int PNo { get; set; }
        [Required]
        public string School { get; set; }
        [Required]
        public string College { get; set; }
    }
}