﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CollegeWizard.ViewModels
{
    public class ViewModel
    {
        public class Student
        {
            public int Id { get; set; }
            public int Name { get; set; }
        }
        public class Details
        {
            public string Address { get; set; }
            public string Phone { get; set; }
        }

    }

}