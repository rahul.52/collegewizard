﻿using CollegeWizard.Models;
using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CollegeWizard.Controllers
{
    public class CustomerController : Controller
    {
        
        public ActionResult Index()
        {
            return View("BasicDetails");
        }


        private Customer GetCustomer()
        {
            if (Session["customer"] == null)
            {
                Session["customer"] = new Customer();
            }
            return (Customer)Session["customer"];
        }

        private void RemoveCustomer()
        {
            Session.Remove("customer");
        }

        [HttpPost]
 
        public ActionResult BasicDetails(BasicDetails data, string prevBtn, string nextBtn)
        {
            Customer obj = GetCustomer();

            if (nextBtn != null)
            {
                AddressDetails ad = new AddressDetails();
                ad.Address = obj.Address;
                ad.City = obj.City;
                ad.Country = obj.Country;
                ad.PostalCode = obj.PostalCode;

                if (ModelState.IsValid)
                {
                   obj.CustomerID = data.CustomerID;
                   obj.CompanyName = data.CompanyName;

                    return View("AddressDetails", ad);
                }
            }
            return View();
        }
        [HttpPost]
        public ActionResult AddressDetails(AddressDetails data, string prevBtn, string nextBtn)
        {
            Customer obj = GetCustomer();

            if (prevBtn != null)
            {
                obj.Address = data.Address;
                obj.City = data.City;
                obj.Country = data.Country;
                obj.PostalCode = data.PostalCode;

                BasicDetails bd = new BasicDetails();
                bd.CustomerID = obj.CustomerID;
                bd.CompanyName = obj.CompanyName;
                return View("BasicDetails", bd);
            }
            if (nextBtn != null)
            {

                ContactDetails cd = new ContactDetails();
                cd.ContactName = obj.ContactName;
                cd.Fax = obj.Fax;
                cd.Phone = obj.Phone;

                if (ModelState.IsValid)
                {
                    obj.Address = data.Address;
                    obj.City = data.City;
                    obj.Country = data.Country;
                    obj.PostalCode = data.PostalCode;

                    return View("ContactDetails", cd);
                }
            }
            return View();
        }
        [HttpPost]
        public ActionResult ContactDetails(ContactDetails data, string prevBtn, string nextBtn)
        {
            Customer obj = GetCustomer();

            if (prevBtn != null)
            {
                obj.ContactName = data.ContactName;
                obj.Phone = data.Phone;
                obj.Fax = data.Fax;

                AddressDetails ad = new AddressDetails();
                ad.Address = obj.Address;
                ad.City = obj.City;
                ad.Country = obj.Country;
                ad.PostalCode = obj.PostalCode;
                return View("AddressDetails", ad);
            }
            if (nextBtn != null)
            {
                if (ModelState.IsValid)
                {
                    obj.ContactName = data.ContactName;
                    obj.Phone = data.Phone;
                    obj.Fax = data.Fax;

                    using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                    {
                        db.Query<Customer>(@"INSERT INTO Customer (CustomerID, CompanyName, ContactName, Address,City,Country,PostalCode,Phone, Fax ) VALUES (@CustomerID, @CompanyName, @ContactName, @Address, @City, @Country, @PostalCode, @Phone, @Fax )", new { obj.CustomerID, obj.CompanyName, obj.ContactName, obj.Address, obj.City, obj.Country, obj.PostalCode, obj.Phone, obj.Fax });
                    }
                    RemoveCustomer();
                    return View("Success");
                }
            }
            return View();
        }

    }
}