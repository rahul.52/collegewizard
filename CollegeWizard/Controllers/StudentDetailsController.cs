﻿using Dapper;
using CollegeWizard.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CollegeWizard.Controllers
{
    public class StudentDetailsController : Controller
    {


        // GET: StudentDetails
        public ActionResult Index()
        {
            return View("Student");
        }

        private StudentDetail GetStudentDetail()
        {
            if (Session["studentDetail"] == null)
            {
                Session["studentDetail"] = new StudentDetail();
            }
            return (StudentDetail)Session["studentDetail"];
        }

        private void RemoveStudentDetail()
        {
            Session.Remove("studentDetail");
        }

        [HttpPost]
        public ActionResult Student(Student student, string BtnNext)
        {
            if (ModelState.IsValid)

            {
                if (BtnNext != null)
                {
                    StudentDetail studObj = GetStudentDetail();

                    studObj.Name = student.Name;

                    return View("Details");
                }
            }
            return View();
        }



        [HttpPost]
        public ActionResult Details(Details details, string BtnSave)
        {
            StudentDetail studObj = GetStudentDetail();

            //if (BtnPrevious != null)
            //{
            //    Student stu = new Student();

            //    studObj.Name = stu.Name;

            //}

            if (ModelState.IsValid)

            {
                if (BtnSave != null)
                {
                    studObj.Address = details.Address;
                    studObj.Phone = details.Phone;
                    using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                    {
                        db.Query<StudentDetail>(@"INSERT INTO StudentDetail (Name, Address, Phone) VALUES (@Name, @Address, @Phone)", new { studObj.Name, studObj.Address, studObj.Phone });

                    }
                    RemoveStudentDetail();
                    return View("Success");
                }      
            }
            return View();
        }

    }
}