﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CollegeWizard.Models;
using Dapper;

namespace CollegeWizard.Controllers
{
    public class PerAccController : Controller
    {
        // GET: PerAcc
        public ActionResult Index(string BtnCalc)
        {
            List<Gender> genderList = new List<Gender>();
            List<District> districtList = new List<District>();
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                genderList = db.Query<Gender>("Select * From Gender").ToList();
                districtList = db.Query<District>("Select * From District").ToList();
            }
            ViewBag.GId = new SelectList(genderList, "GId", "GName");
            ViewBag.DId = new SelectList(districtList, "DId", "DistrictName");

            if (BtnCalc != null)
            {
                return View("PersonDetail");
            }
            return View();
        }


        private PerAcc GetPersonAccount()
        {
            if (Session["PersonAccount"] == null)
            {
                Session["PersonAccount"] = new PerAcc();
            }
            return (PerAcc)Session["PersonAccount"];
        }

        private void RemovePersonAccount()
        {
            Session.Remove("PersonAccount");
        }

        [HttpPost]
        public ActionResult PersonDetail(PersonDetail acc, string prevBtn, string nextBtn)
        {
            PerAcc obj = GetPersonAccount();

            List<Gender> genderList = new List<Gender>();
            List<District> districtList = new List<District>();
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                genderList = db.Query<Gender>("Select * From Gender").ToList();
                districtList = db.Query<District>("Select * From District").ToList();
            }
            ViewBag.GId = new SelectList(genderList, "GId", "GName");
            ViewBag.DId = new SelectList(districtList, "DId", "DistrictName");

            if (nextBtn != null)
            {               
                PersonAddress pa = new PersonAddress();
                pa.Phone = obj.Phone;
                pa.Country = obj.Country;
                pa.Salary = obj.Salary;
                pa.DId = obj.DId;
                ViewBag.DId = new SelectList(districtList, "DId", "DistrictName", pa.DId);

                if (ModelState.IsValid)
                {
                    obj.Name = acc.Name;
                    obj.GId = acc.GId;
                    return View("PersonAddress", pa);
                }
            }
            return View();
        }

        [HttpPost]
        public ActionResult PersonAddress(PersonAddress acc, string prevBtn, string nextBtn)
        {
            PerAcc obj = GetPersonAccount();

            List<Gender> genderList = new List<Gender>();
            List<District> districtList = new List<District>();
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                genderList = db.Query<Gender>("Select * From Gender").ToList();
                districtList = db.Query<District>("Select * From District").ToList();
            }
            ViewBag.GId = new SelectList(genderList, "GId", "GName");
            ViewBag.DId = new SelectList(districtList, "DId", "DistrictName");

            if (prevBtn != null)
            {
                obj.Phone = acc.Phone;
                obj.Country = acc.Country;
                obj.Salary = acc.Salary;
                obj.DId = acc.DId;

                PersonDetail pd = new PersonDetail();
                pd.Name = obj.Name;
                pd.GId = obj.GId;
                ViewBag.GId = new SelectList(genderList, "GId", "GName", pd.GId);
                return View("PersonDetail", pd);
            }

            if (nextBtn != null)
            {
                Study s = new Study();
                s.School = obj.School;
                s.College = obj.College;

                if (ModelState.IsValid)
                {
                    obj.Phone = acc.Phone;
                    obj.Country = acc.Country;
                    obj.DId = acc.DId;
                    obj.Salary = acc.Salary;
                    return View("Study", s);
                }
            }
            return View();
        }
        [HttpPost]
        public ActionResult Study(Study acc, string prevBtn, string nextBtn)
        {
            PerAcc obj = GetPersonAccount();

            List<Gender> genderList = new List<Gender>();
            List<District> districtList = new List<District>();
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                genderList = db.Query<Gender>("Select * From Gender").ToList();
                districtList = db.Query<District>("Select * From District").ToList();
            }
            ViewBag.GId = new SelectList(genderList, "GId", "GName");
            ViewBag.DId = new SelectList(districtList, "DId", "DistrictName");
            
            if (prevBtn != null)
            {
                obj.School = acc.School;
                obj.College = acc.College;

                PersonAddress pa = new PersonAddress();
                pa.Phone = obj.Phone;
                pa.Country = obj.Country;
                pa.Salary = obj.Salary;
                pa.DId = obj.DId;
                ViewBag.DId = new SelectList(districtList, "DId", "DistrictName",pa.DId);
                return View("PersonAddress", pa);
            }

            if (nextBtn != null)
            {
                Job j = new Job();
                j.Office = obj.Office;
                j.Department = obj.Department;

                if (ModelState.IsValid)
                {
                    obj.School = acc.School;
                    obj.College = acc.College;
                    return View("Job", j);
                }
            }
            return View();
        }
        [HttpPost]
        public ActionResult Job(Job acc, string prevBtn, string nextBtn)
        {
            PerAcc obj = GetPersonAccount();
            if (prevBtn != null)
            {
                obj.Office = acc.Office;
                obj.Department = acc.Department;

                Study s = new Study();
                s.School = obj.School;
                s.College = obj.College;
                return View("Study", s);

            }
            if (nextBtn != null)
            {
                if (ModelState.IsValid)
                {
                    obj.Office = acc.Office;
                    obj.Department = acc.Department;

                    int pnum;
                    try
                    {
                        using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                        {
                            pnum = db.Query<PersonDetail>(@"select Top 1 PNo from PersonDetail Order By PNo Desc;").SingleOrDefault().PNo;

                        }
                    }
                    catch (Exception Ex)
                    {
                        pnum = 5000;
                    }
                    string gen;
                    string dis;
                    using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                    {
                        gen = db.Query<Gender>(@"select * from Gender where GId = @GId", new { obj.GId }).SingleOrDefault().GName;
                        dis = db.Query<District>(@"select * from District where DId = @DId", new { obj.DId }).SingleOrDefault().DistrictName;

                    }
                    Random rnd = new Random();
                    int rand = rnd.Next(1, 60);
                    int snum = pnum + rand;
                    using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                    {
                        db.Query<PersonDetail>(@"INSERT INTO PersonDetail (PNo, GNAme, Name ) VALUES (@snum, @gen, @Name )", new { snum, gen, obj.Name });
                        db.Query<PersonAddress>(@"INSERT INTO PersonAddress (PNo, Phone, DistrictName ,Country ) VALUES (@snum, @Phone,@dis,@Country )", new { snum, obj.Phone, dis, obj.Country });
                        db.Query<Study>(@"INSERT INTO Study (PNo, School, College ) VALUES (@snum, @School,@College )", new { snum, obj.School, obj.College });
                        db.Query<Job>(@"INSERT INTO Job (PNo, Office, Department,Salary ) VALUES (@snum, @Office, @Department,@Salary )", new { snum, obj.Office, obj.Department, obj.Salary });
                    }
                    RemovePersonAccount();
                    return View("Success");
                }
            }
            return View();
        }

    }
}






